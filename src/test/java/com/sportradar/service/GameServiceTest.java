package com.sportradar.service;

import com.sportradar.data.Game;
import com.sportradar.data.GameId;
import com.sportradar.data.NationalTeam;
import com.sportradar.data.ScoreBoard;
import com.sportradar.data.Sport;
import com.sportradar.data.Stage;
import com.sportradar.repository.GameRepository;
import com.sportradar.repository.ScoreBoardRepository;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class GameServiceTest {

    private static final Clock TEST_CLOCK = Clock.fixed(Instant.now(), ZoneOffset.UTC);
    private GameRepository gameRepository = mock(GameRepository.class);
    private ScoreBoardRepository scoreBoardRepository = mock(ScoreBoardRepository.class);
    private GameService gameService = new GameService(gameRepository, scoreBoardRepository, TEST_CLOCK);

    @Test
    void shouldStartTheGame() {
        NationalTeam homeTeam = new NationalTeam("arg", "Argentina", Sport.FOOTBALL);
        NationalTeam awayTeam = new NationalTeam("fra", "France", Sport.FOOTBALL);
        GameId gameId = new GameId("wc2022", Stage.FINAL, homeTeam.id(), awayTeam.id());

        Game mock = mock(Game.class);
        when(mock.getId())
                .thenReturn(gameId);
        when(gameRepository.save(any(Game.class)))
                .thenReturn(mock);

        gameService.startGame(homeTeam, awayTeam, "wc2022", Stage.FINAL);

        verify(gameRepository, times(1)).save(any(Game.class));
        verify(scoreBoardRepository, times(1)).save(any(ScoreBoard.class));
    }
}