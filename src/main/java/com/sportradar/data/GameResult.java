package com.sportradar.data;

import java.time.Instant;

public class GameResult {
    private String id;
    private String gameId;
    private Integer homeScore;
    private Integer awayScore;
    private Instant dateTime;
}
