package com.sportradar.data;

public enum GameStatus {
    SCHEDULED, STARTED, CANCELED, FINISHED
}
