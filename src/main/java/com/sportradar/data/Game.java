package com.sportradar.data;

import java.time.Instant;
import java.util.Objects;

public class Game {
    private GameId id;
    private Sport sport;
    private NationalTeam homeTeam;
    private NationalTeam awayTeam;
    private String tournamentId;
    private Stage stage;
    private Instant dateTime;

    public Game(
            GameId id,
            Sport sport,
            NationalTeam homeTeam,
            NationalTeam awayTeam,
            String tournamentId,
            Stage stage,
            Instant dateTime
    ) {
        this.id = id;
        this.sport = sport;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.dateTime = dateTime;
        this.tournamentId = tournamentId;
        this.stage = stage;
    }

    public GameId getId() {
        return id;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public NationalTeam getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(NationalTeam homeTeam) {
        this.homeTeam = homeTeam;
    }

    public NationalTeam getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(NationalTeam awayTeam) {
        this.awayTeam = awayTeam;
    }

    public Instant getDateTime() {
        return dateTime;
    }

    public void setDateTime(Instant dateTime) {
        this.dateTime = dateTime;
    }

    public String getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(String tournamentId) {
        this.tournamentId = tournamentId;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(id, game.id) && sport == game.sport && Objects.equals(homeTeam, game.homeTeam) && Objects.equals(awayTeam, game.awayTeam) && Objects.equals(tournamentId, game.tournamentId) && stage == game.stage && Objects.equals(dateTime, game.dateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sport, homeTeam, awayTeam, tournamentId, stage, dateTime);
    }
}
