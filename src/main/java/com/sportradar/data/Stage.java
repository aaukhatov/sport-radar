package com.sportradar.data;

public enum Stage {
    GROUP, ROUND_16, QUARTER_FINAL, SEMI_FINAL, FINAL
}
