package com.sportradar.data;

import java.io.Serializable;
import java.util.Objects;

public final class GameId implements Serializable {
    private String tournament;
    private Stage stage;
    private String homeTeam;
    private String awayTeam;

    public GameId(String tournament, Stage stage, String homeTeam, String awayTeam) {
        this.tournament = tournament;
        this.stage = stage;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }

    public String getTournament() {
        return tournament;
    }

    public Stage getStage() {
        return stage;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameId gameId = (GameId) o;
        return Objects.equals(tournament, gameId.tournament) && stage == gameId.stage && Objects.equals(homeTeam, gameId.homeTeam) && Objects.equals(awayTeam, gameId.awayTeam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tournament, stage, homeTeam, awayTeam);
    }

    @Override
    public String toString() {
        // naive hash as a key
        return "%s.%s.%s.%s".formatted(tournament, stage, homeTeam, awayTeam);
    }
}
