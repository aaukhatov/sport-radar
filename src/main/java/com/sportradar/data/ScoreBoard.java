package com.sportradar.data;

import java.util.Objects;

public class ScoreBoard {
    private String id;
    private Game game;
    private Integer homeScore;
    private Integer awayScore;

    public ScoreBoard(String id, Game game, Integer homeScore, Integer awayScore) {
        this.id = id;
        this.game = game;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
    }

    public String getId() {
        return id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Integer getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(Integer homeScore) {
        this.homeScore = homeScore;
    }

    public Integer getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(Integer awayScore) {
        this.awayScore = awayScore;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (ScoreBoard) obj;
        return Objects.equals(this.id, that.id) &&
                Objects.equals(this.game, that.game) &&
                Objects.equals(this.homeScore, that.homeScore) &&
                Objects.equals(this.awayScore, that.awayScore);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, game, homeScore, awayScore);
    }

    @Override
    public String toString() {
        return "ScoreBoard[" +
                "id=" + id + ", " +
                "game=" + game + ", " +
                "homeScore=" + homeScore + ", " +
                "awayScore=" + awayScore + ']';
    }

}
