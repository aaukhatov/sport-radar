package com.sportradar.data;

import java.util.Objects;

public class NationalTeam {
    private String id;
    private String country;
    private Sport sport;

    public NationalTeam(String id, String country, Sport sport) {
        this.id = id;
        this.country = country;
        this.sport = sport;
    }

    public String id() {
        return id;
    }

    public String country() {
        return country;
    }

    public Sport sport() {
        return sport;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (NationalTeam) obj;
        return Objects.equals(this.id, that.id) &&
                Objects.equals(this.country, that.country) &&
                Objects.equals(this.sport, that.sport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, country, sport);
    }

    @Override
    public String toString() {
        return "NationalTeam[" +
                "id=" + id + ", " +
                "country=" + country + ", " +
                "sport=" + sport + ']';
    }

}
