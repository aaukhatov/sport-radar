package com.sportradar.service;

import com.sportradar.data.Game;
import com.sportradar.data.GameId;
import com.sportradar.data.NationalTeam;
import com.sportradar.data.ScoreBoard;
import com.sportradar.data.Sport;
import com.sportradar.data.Stage;
import com.sportradar.repository.GameRepository;
import com.sportradar.repository.ScoreBoardRepository;

import java.time.Clock;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

public class GameService {

    private final GameRepository gameRepository;
    private final ScoreBoardRepository scoreBoardRepository;
    private final Clock clock;

    public GameService(GameRepository gameRepository,
                       ScoreBoardRepository scoreBoardRepository,
                       Clock clock) {

        this.gameRepository = gameRepository;
        this.scoreBoardRepository = scoreBoardRepository;
        this.clock = clock;
    }

    public GameId startGame(NationalTeam home, NationalTeam away, String tournament, Stage stage) {

        GameId gameId = new GameId(tournament, stage, home.id(), away.id());

        Game game = gameRepository.save(new Game(gameId, Sport.FOOTBALL, home, away, tournament, stage, Instant.now(clock)));

        scoreBoardRepository.save(new ScoreBoard(UUID.randomUUID().toString(), game, 0, 0));

        return game.getId();
    }

    public void updateGameScore(String gameId, int homeTeamScore, int awayTeamScore) {
        ScoreBoard match = scoreBoardRepository.findByGameId(gameId);
        match.setHomeScore(homeTeamScore);
        match.setAwayScore(awayTeamScore);

        scoreBoardRepository.update(match);
    }

    public void finishGame(String gameId) {
        ScoreBoard liveMatch = scoreBoardRepository.findByGameId(gameId);
        Game game = liveMatch.getGame();
        gameRepository.save(game);
        scoreBoardRepository.delete(liveMatch);
    }

    public List<ScoreBoard> getLiveSummary() {
        return scoreBoardRepository.getSummary();
    }
}
