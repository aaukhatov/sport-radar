package com.sportradar.repository;

import com.sportradar.data.Game;

public interface GameRepository {
    Game save(Game game);

    Game update(Game game);

    void delete(Game game);

    Game findById(String id);
}
