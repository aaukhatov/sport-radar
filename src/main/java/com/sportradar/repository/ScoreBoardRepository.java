package com.sportradar.repository;

import com.sportradar.data.ScoreBoard;

import java.util.List;

public interface ScoreBoardRepository {

    List<ScoreBoard> getSummary();

    ScoreBoard save(ScoreBoard board);

    ScoreBoard update(ScoreBoard board);

    void delete(ScoreBoard board);

    ScoreBoard findByGameId(String gameId);
}
