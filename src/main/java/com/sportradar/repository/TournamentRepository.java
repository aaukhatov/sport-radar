package com.sportradar.repository;

import com.sportradar.data.Tournament;

public interface TournamentRepository {
    Tournament findById(String id);
    Tournament save(Tournament tournament);
}
