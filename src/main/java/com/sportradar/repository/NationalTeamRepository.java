package com.sportradar.repository;

import com.sportradar.data.NationalTeam;
import com.sportradar.data.Sport;

public interface NationalTeamRepository {

    NationalTeam findByCountryAndSport(String country, Sport sport);
}
